var Options =
{
	webserver:
	{
		port: 8080
	},
	hylafax:
	{
		host: 'hylafax_server'
		,port: 4559
		,username: 'username'
		,password: 'password'
	}
};

exports.Options = Options;