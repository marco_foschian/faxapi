# faxApi#

A web api for hylafax server and also a minimal GUI

# Installation #

* clone the repository
```
git clone https://marco_foschian@bitbucket.org/marco_foschian/faxapi.git
```

* install dependencies
```
cd faxapi
npm install
```

* Edit the configuration file (Options.js) for setting web listening port and hylafax server address
* start the server
```
node server.js
```
* access the web interface

# Methods #

## /modems ##

Get a json with the modem status

## /jobs ##

Get a json with the jobs status

## /jobs/active ##

Get a json with only the active jobs

## /jobs/completed ##

Get a json with only the completed jobs

## /jobs/archived ##

Get a json with only the archived jobs

## /sendfax [POST] ##

A post to this url trigger a fax send. The accepted fields are:

* faxnumber - the dialstring where to send the fax
* files - the file sent with multipart/form-data style

## /jobs/{job_id} ##

Get the job info for the specified id. (For Example GET http://myserver:8080/jobs/75

## /jobs/{job_id}/kill ##

Kill the specified job (must be active)







