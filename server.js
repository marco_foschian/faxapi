var minihttp = require('minihttp');
var util = require('util');
var fs = require('fs');
var Hylafax = require('hylafax-clip');
var options = require('./Options').Options;

var Promise = require( 'promise' );

var hoptions = options.hylafax;


//--- Formatters ---
//{
function pad2( n ) {
	return (n > 10 ? '' : '0') + n;
}
function logDate() {
	var d = new Date();
	var dt = [d.getFullYear(), pad2(d.getMonth() + 1 ), pad2(d.getDate())].join('-');
	var tm = [pad2(d.getHours()), pad2(d.getMinutes()), pad2(d.getSeconds())].join(':');
	return dt+' '+tm;
}
//}

//---------------------
//	HYLAFAX Logic
//{
function faxTransaction( callback )
{
	if( !callback || typeof callback != 'function' )
		return;

	var fax = new Hylafax( hoptions ); // TODO: take from a pool !
	return fax.connect().then( function()
	{
		return callback( fax );
	})
	.then( function( result )
	{
		fax.disconnect();
		return result;
	},
	function( err )
	{
		console.log( '%s faxTransaction ERR: %s', logDate(), err );
		return err;
	});
}

function faxmodems()
{
	return faxTransaction( function(fax)
	{
		return fax.getModemStatus();
	});
}


function faxJobs( status )
{
	return faxTransaction( function( fax ) 
	{
		var s = status || '*';
		var jobs = {};
		var p = Promise.resolve();

		if( s == 'active' || s == '*' )
		{
			p = p.then( function()
			{
				return fax.getActiveJobs().then( function( js )
				{
					//console.log( util.inspect( js ) );
					jobs['active'] = js;
				});
			});
		}
		if( s == 'archived' || s == '*' )
		{
			p = p.then( function()
			{
				return fax.getArchivedJobs().then( function( js )
				{
					jobs['archived'] = js;
				});
			});
		}
		if( s == 'completed' || s == '*' )
		{
			p = p.then( function()
			{
				return fax.getCompletedJobs().then( function( js )
				{
					jobs['completed'] = js;
				});
			});
		}	
		return p.then( function()
		{
			if( s != '*' )
				return jobs[s];
			else
				return jobs;
		});
	});
}

function faxsend( faxnum, faxpath )
{
	var opts =
	{
		number: faxnum
		,user: hoptions.username
		//,debug: debugLog
	};

	console.log( '%s Sending a fax: %s - %s', logDate(), faxnum, faxpath );
	return faxTransaction( function(fax)
	{
		//console.log( '%s Transaction opened', logDate() );
		return fax.sendFax(opts, fs.createReadStream( faxpath ))
			.then( function(data) {
				console.log( '%s Fax sent: %s', logDate(), util.inspect(data) );
				return data;
			});
	});
}

function faxkill( job_id, adminPass )
{
	return faxTransaction( function(fax)
	{
		return fax.killFax( job_id, adminPass );
	});
}

function faxcheck( job_id )
{
	return faxTransaction( function(fax)
	{
		return fax.checkJob( job_id );
	});
}

function faxcheckJobset( jobset_id )
{
	var path = 'jobsets/'+jobset_id;
	return Data.read( path ).then( function( data ) {

		return faxJobs( '*' ).then( function( jobs ) {
			
			var sets = ['active','completed','archived'];
			var jj = data.jobs.map( function( job_id ) {

				for( var i=0; i<sets.length; i++ ) {
					var status = sets[i];
					var items = jobs[status];
					if( items ) {
						var j = items[job_id];
						if( j ) {
							return { id: job_id, job: j, status: status };
						}
					}
				}
				
				return null; // Not found !
			});
			
			return jj;
		});
	});
}


//}




//---------------------
//	DataTable
//{
function tabelize( obj )
{
	var keys = Object.keys( obj ).sort();
	var data = [];
	for( var i=0; i<keys.length; i++ )
	{
		var key = keys[i];
		var item = obj[key];
		if( typeof( item ) == 'object' )
			data.push( item );
	}
	return data;
}

//---------------------
//}



//---------------------
//	HTTP server
//{
var Server = new minihttp.HttpServer( options.webserver );

Server.route('/modems',
{
	get: function (request, response, parms )
	{
		return faxmodems();
	}
});

Server.route('/jobs/completed',
{
	get: function (request, response, parms )
	{
		return faxJobs('completed')
	}
});

Server.route('/jobs/completed/t',
{
	get: function (request, response, parms )
	{
		return faxJobs('completed').then( tabelize );
	}
});

Server.route('/jobs/active',
{
	get: function (request, response, parms )
	{
		return faxJobs('active');
	}
});

Server.route('/jobs/active/t',
{
	get: function (request, response, parms )
	{
		return faxJobs('active').then( tabelize );
	}
});

Server.route('/jobs/archived',
{
	get: function (request, response, parms )
	{
		return faxJobs('archived');
	}
});

Server.route('/jobs',
{
	get: function (request, response, parms )
	{
		return faxJobs('*');
	}
});

Server.route('/sendfax',
{
	post: function (request, response, parms )
	{
		var faxdest = parms.fields.faxnumber;
		var file = parms.files[0];
		var fileName = (file ? file.path : 'no file' );
		
		//console.log( util.inspect( parms.fields ) );
		//console.log( util.inspect( parms.files ) );
		//console.log( '%s Uploaded file path %s', logDate(), fileName );
		var singleDest = false;
		if( typeof( faxdest ) == 'string' && faxdest.indexOf(';') < 0 )
			singleDest = true;
		
		return faxsend( faxdest, fileName )
		.then( function( data )
		{
			// delete file once sent to fax server
			fs.unlinkSync(fileName)
			if( singleDest == false )
				return data;
			else
				return data[0];
		},
		function(err) {
			// delete file once sent to fax server
			fs.unlinkSync(fileName)
			console.log( "%s ERROR sending fax to %s: %s", logDate(), faxdest, err );
		})
	}
});

Server.route('/jobs/{job_id}',
{
	get: function (request, response, parms )
	{
		var job_id = parms.ids.job_id;
		
		return faxcheck( job_id );
	}
});

Server.route('/jobs/{job_id}/kill',
{
	get: function (request, response, parms )
	//post: function (request, response, parms )
	{
		var job_id = parms.ids.job_id;
		var adminId = parms.fields.apwd;
		
		return faxkill( job_id, adminId );
	}
});

var Data = {
	baseDir: './data/',
	ext: 'json',
	mapPath: function( name ) {
		return Data.baseDir + name;
	},
	getItems: function( folderName, extension ) {

		return new Promise( function(resolve, reject) {

			var r = new RegExp( "^(.+)\."+ (extension || Data.ext) +"$" );

			fs.readdir( Data.mapPath( folderName ), function(err, files) {
				if( err )
					reject( err );
				else {
					var res = [];

					files.forEach( function(file) {
						var m = file.match( r );
						if( m )
							res.push( m[1] );
					});

					resolve( res );
				}
			});
		});
	},
	save: function( path, object ) {

		return new Promise( function(resolve, reject) {

			var fname = Data.mapPath( path ) + '.' + Data.ext;
			var content = JSON.stringify( object );

			fs.writeFile( fname, content, function (err) {
				if( err ) {
					console.log( '%s ERROR in Data.save: %s', logDate(), err );
					reject( err );
				}
				else
					resolve();
			});
		});
	},
	read: function( path, callback ) {

		return new Promise( function(resolve, reject) {

			var fname = Data.mapPath( path ) + '.' + Data.ext;

			fs.readFile( fname, 'utf8', function(err, data) {
				if( err ) {
					console.log( '%s ERROR in Data.save: %s', logDate(), err );
					reject( err );
				}
				else {
					var obj = JSON.parse( data );
					resolve( obj );
				}
			}); 
		});
	}
}

Server.route('/jobsets',
{
	get: function (request, response, parms )
	//post: function (request, response, parms )
	{
		return Data.getItems( 'jobsets', 'json' );
	}
});

Server.route('/jobsets/{id}',
{
	get: function (request, response, parms )
	//post: function (request, response, parms )
	{
		var id = parms.ids.id;
		var path = 'jobsets/'+id;
		return Data.read( path );
	}
});

Server.route('/jobsets/{id}/status',
{
	get: function (request, response, parms )
	//post: function (request, response, parms )
	{
		var id = parms.ids.id;
		return faxcheckJobset( id );
	}
});


Server.route('/groups',
{
	get: function (request, response, parms )
	//post: function (request, response, parms )
	{
		return Data.getItems( 'jobsets', 'json' );
	}
});

Server.route('/groups/{id}',
{
	get: function (request, response, parms )
	//post: function (request, response, parms )
	{
		var id = parms.ids.id;
		var path = 'groups/'+id;
		return Data.read( path );
	}
});


Server.route('/groups/{id}/sendfax',
{
	post: function (request, response, parms )
	{
		var id = parms.ids.id;
		var gpath = 'groups/'+id;

		if( !parms.files || !parms.files[0] ) {
			Server.sendErrorResponse( response, 'No file to send' );
			return;
		}
		var file = parms.files[0];
		var fileName = file.path;
		
		var jobset = { file: fileName, group: id, _t: new Date() };
		
		Data.read( gpath ).then( function(data) {
		
			var targets = data.filter( function(c) { return c.fax ? true : false; } )
				.map( function(c) { return c.fax; } );

			jobset.targets = targets;

			// return faxsend( targets, fileName );
			return targets;
		})
		.then( function( data ) {
			jobset.jobs = data;

			// delete file once sent to fax server
			fs.unlinkSync(fileName)

			// save jobset
			var name = 'test-jobset-001';
			return Data.save( 'jobsets/'+name, jobset ).then( function() {
				return name;
			});
		})
		.then( function( job_name ) {
			Server.redirectToUrl( response, '/jobsets/'+job_name+'/status' );
		},
		function( err ) {
			// delete file once sent to fax server
			fs.unlinkSync(fileName)	
			console.log( '%s ERROR sending fax to group %s: %s', logDate(), id, err );
			Server.sendErrorResponse( response, err.toString() );
		});

	}
});


Server.listen();

//}
